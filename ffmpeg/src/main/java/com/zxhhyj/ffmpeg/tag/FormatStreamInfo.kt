package com.zxhhyj.ffmpeg.tag

public data class FormatStreamInfo(
    val cover: ByteArray? = null,
    val duration: Long,
    val bitrate: Long,
    val bitsPerRawSample: Int,
    val sampleRate: Long,
    val channels: Int,
    val metadata: Map<String, String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FormatStreamInfo

        if (cover != null) {
            if (other.cover == null) return false
            if (!cover.contentEquals(other.cover)) return false
        } else if (other.cover != null) return false
        if (duration != other.duration) return false
        if (bitrate != other.bitrate) return false
        if (bitsPerRawSample != other.bitsPerRawSample) return false
        if (sampleRate != other.sampleRate) return false
        if (channels != other.channels) return false
        if (metadata != other.metadata) return false

        return true
    }

    override fun hashCode(): Int {
        var result = cover?.contentHashCode() ?: 0
        result = 31 * result + duration.hashCode()
        result = 31 * result + bitrate.hashCode()
        result = 31 * result + bitsPerRawSample
        result = 31 * result + sampleRate.hashCode()
        result = 31 * result + channels
        result = 31 * result + metadata.hashCode()
        return result
    }
}
