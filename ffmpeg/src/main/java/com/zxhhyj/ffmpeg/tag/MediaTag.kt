package com.zxhhyj.ffmpeg.tag

import com.zxhhyj.ffmpeg.utils.LibraryUtils
import java.io.File

public object MediaTag {

    public external fun getMediaFormatStreamInfo(path: String): FormatStreamInfo?

    public fun getMediaFormatStreamInfo(file: File): FormatStreamInfo? {
        return getMediaFormatStreamInfo(file.path)
    }

    init {
        LibraryUtils.loadLibrary()
    }

}