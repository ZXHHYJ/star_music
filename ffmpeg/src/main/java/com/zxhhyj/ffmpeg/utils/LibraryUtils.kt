package com.zxhhyj.ffmpeg.utils

public object LibraryUtils {
    public fun loadLibrary() {
        //nothing
    }

    init {
        System.loadLibrary("avcodec")
        System.loadLibrary("avdevice")
        System.loadLibrary("avformat")
        System.loadLibrary("avutil")
        System.loadLibrary("swresample")
        System.loadLibrary("ffmpeg")
    }
}