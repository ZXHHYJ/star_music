#ifndef HASHMAP_H
#define HASHMAP_H

#include <jni.h>

class hashmap {
public:
    hashmap(JNIEnv *env) {
        this->env = env;
        hashMapClass = env->FindClass("java/util/HashMap");
        hashMapConstructor = env->GetMethodID(hashMapClass, "<init>", "()V");
        hashMapPutMethod = env->GetMethodID(hashMapClass, "put",
                                            "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
        jHashMap = env->NewObject(hashMapClass, hashMapConstructor);
    }

    void put(jobject key, jobject value) {
        env->CallObjectMethod(jHashMap, hashMapPutMethod, key, value);
    }

    jobject getJavaObject() {
        return jHashMap;
    }

private:
    jclass hashMapClass;
    jmethodID hashMapConstructor;
    jmethodID hashMapPutMethod;
    jobject jHashMap;
    JNIEnv *env;
};

#endif // HASHMAP_H