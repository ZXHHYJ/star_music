package com.zxhhyj.ui.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.unit.IntSize

val LocalTopBarState = compositionLocalOf { TopBarState() }

private val NothingPaddingValues = PaddingValues()

@Composable
fun AppScaffold(
    modifier: Modifier,
    contentModifier: Modifier = Modifier,
    insetPaddingValues: PaddingValues = NothingPaddingValues,
    topBar: (@Composable () -> Unit)? = null,
    bottomNavigationBar: @Composable (ColumnScope.() -> Unit)? = null,
    content: @Composable (PaddingValues) -> Unit
) {
    Box(
        modifier = modifier
    ) {
        CompositionLocalProvider(LocalTopBarState provides rememberSaveable { TopBarState() }) {
            var bottomBarSize by remember {
                mutableStateOf(IntSize.Zero)
            }
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .then(contentModifier)
                    .bindTopBarState()
                    .padding(
                        bottom = with(LocalDensity.current) {
                            LocalTopBarState.current.topBarActiveLength.toDp()
                        }
                    )
            ) {
                content(
                    PaddingValues(
                        top = insetPaddingValues.calculateTopPadding(),
                        bottom = insetPaddingValues.calculateBottomPadding() + with(LocalDensity.current) {
                            bottomBarSize.height.toDp()
                        },
                        start = insetPaddingValues.calculateLeftPadding(LocalLayoutDirection.current),
                        end = insetPaddingValues.calculateRightPadding(LocalLayoutDirection.current)
                    )
                )
            }
            topBar?.invoke()
            if (bottomNavigationBar != null)
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.BottomCenter)
                        .onSizeChanged {
                            bottomBarSize = it
                        }) {
                    bottomNavigationBar()
                }
        }
    }
}