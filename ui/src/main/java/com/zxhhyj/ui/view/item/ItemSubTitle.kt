package com.zxhhyj.ui.view.item

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LocalContentColor
import androidx.compose.material.LocalTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import com.zxhhyj.ui.theme.LocalColorScheme
import com.zxhhyj.ui.theme.LocalDimens
import com.zxhhyj.ui.theme.LocalTextStyles

@Composable
fun ItemSubTitle(title: @Composable () -> Unit) {
    Box(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = LocalDimens.current.horizontal)
            .padding(
                horizontal = LocalDimens.current.horizontal,
                vertical = LocalDimens.current.vertical
            )
    ) {
        CompositionLocalProvider(
            LocalTextStyle provides LocalTextStyles.current.sub,
            LocalContentColor provides LocalColorScheme.current.subText
        ) {
            title()
        }
    }
}