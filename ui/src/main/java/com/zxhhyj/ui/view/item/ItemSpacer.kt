package com.zxhhyj.ui.view.item

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.zxhhyj.ui.theme.LocalDimens
import com.zxhhyj.ui.theme.StarDimens

@Composable
fun ItemSpacer() {
    Spacer(modifier = Modifier.height(LocalDimens.current.vertical))
}