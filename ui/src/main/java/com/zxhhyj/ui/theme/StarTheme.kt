package com.zxhhyj.ui.theme

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf


val LocalColorScheme =
    compositionLocalOf<StarColorScheme> { throw RuntimeException() }

val LocalTextStyles = compositionLocalOf<StarTextStyles> { throw RuntimeException() }

val LocalDimens = compositionLocalOf<StarDimens> { throw RuntimeException() }

@Composable
fun StarTheme(
    colorScheme: StarColorScheme,
    textStyles: StarTextStyles,
    dimens: StarDimens,
    content: @Composable () -> Unit
) {
    CompositionLocalProvider(
        LocalColorScheme provides colorScheme,
        LocalTextStyles provides textStyles,
        LocalDimens provides dimens
    ) {
        content()
    }
}