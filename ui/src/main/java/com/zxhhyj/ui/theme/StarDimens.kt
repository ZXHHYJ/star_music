package com.zxhhyj.ui.theme

import androidx.compose.runtime.Immutable
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

/**
 * @param vertical 默认垂直边距
 * @param horizontal 默认水平边距
 * @param round 默认圆角大小
 */
@Immutable
data class StarDimens internal constructor(
    val vertical: Dp,
    val horizontal: Dp,
    val round: Dp
)

fun starDimens(
    vertical: Dp = 10.dp,
    horizontal: Dp = 16.dp,
    round: Dp = 8.dp
) = StarDimens(vertical, horizontal, round)