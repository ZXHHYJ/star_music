package com.zxhhyj.ui.theme

import androidx.compose.runtime.Immutable
import androidx.compose.ui.graphics.Color

@Immutable
data class StarColorScheme(
    val highlight: Color,
    val text: Color,
    val onText: Color,
    val subText: Color,
    val background: Color,
    val highBackground: Color,
    val outline: Color,
    val disabled: Color
)

fun starColorScheme(
    highlight: Color = Color(0xFFE41308),
    text: Color = Color.Black,
    onText: Color = Color.White,
    subText: Color = Color(0xFF7A7A7A),
    background: Color = Color(0xFFF1F0F5),
    highBackground: Color = Color.White,
    outline: Color = Color(0xFFDFDFDF),
    disabled: Color = Color(0xFF8D8D8D)
) = StarColorScheme(highlight, text, onText, subText, background, highBackground, outline, disabled)