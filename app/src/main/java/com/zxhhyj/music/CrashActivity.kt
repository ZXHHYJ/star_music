package com.zxhhyj.music

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.SystemBarStyle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.core.view.WindowCompat
import com.zxhhyj.music.ui.theme.StarMusicTheme
import com.zxhhyj.music.ui.theme.isSystemInDarkMode
import com.zxhhyj.ui.theme.LocalColorScheme
import com.zxhhyj.ui.view.AppCenterTopBar
import com.zxhhyj.ui.view.AppScaffold
import com.zxhhyj.ui.view.RoundColumn
import com.zxhhyj.ui.view.item.ItemTint

class CrashActivity : ComponentActivity() {
    companion object {
        const val CRASH_ACTIVITY_KEY = "CRASH_ACTIVITY_KEY"

        /**
         * 启动CrashActivity
         * @param context 上下文
         * @param log 报错信息
         */
        fun startActivity(context: Context, log: String) {
            context.startActivity(Intent(context, CrashActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                putExtra(CRASH_ACTIVITY_KEY, log)
            })
        }
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            StarMusicTheme {
                with(LocalContext.current as ComponentActivity) {
                    if (!isSystemInDarkMode) {
                        enableEdgeToEdge(
                            SystemBarStyle.light(
                                android.R.color.transparent,
                                android.R.color.transparent
                            ),
                            SystemBarStyle.light(
                                android.R.color.transparent,
                                android.R.color.transparent
                            )
                        )
                    } else {
                        enableEdgeToEdge(
                            SystemBarStyle.dark(android.R.color.transparent),
                            SystemBarStyle.dark(android.R.color.transparent)
                        )
                    }
                }
                AppScaffold(
                    topBar = {
                        AppCenterTopBar(title = { Text(text = stringResource(id = R.string.crash_info)) })
                    },
                    modifier = Modifier
                        .fillMaxSize()
                        .background(LocalColorScheme.current.background)
                        .systemBarsPadding()
                        .clipToBounds()
                ) {
                    LazyColumn(modifier = Modifier.fillMaxSize()) {
                        item {
                            RoundColumn {
                                ItemTint {
                                    Text(text = "${intent.extras?.getString(CRASH_ACTIVITY_KEY)}")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}