package com.zxhhyj.music

import android.Manifest
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.core.view.WindowCompat
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState
import com.zxhhyj.music.logic.bean.PlayMemoryBean
import com.zxhhyj.music.logic.repository.AndroidMediaLibRepository
import com.zxhhyj.music.logic.repository.SettingRepository
import com.zxhhyj.music.logic.utils.AudioBecomingNoisyManager
import com.zxhhyj.music.service.playermanager.PlayerManager
import com.zxhhyj.music.ui.common.ComposeToast.ComposeToast
import com.zxhhyj.music.ui.screen.main.MainScreen
import com.zxhhyj.music.ui.theme.StarMusicTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainActivity : ComponentActivity() {

    private val audioBecomingNoisyManager by lazy {
        AudioBecomingNoisyManager(this) {
            PlayerManager.pause()
        }
    }

    @OptIn(ExperimentalPermissionsApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        audioBecomingNoisyManager.setEnabled(true)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            StarMusicTheme {
                MainScreen()
                ComposeToast()
            }
            LaunchedEffect(SettingRepository.EnableEqualizer) {
                PlayerManager.setEnableEqualizer(SettingRepository.EnableEqualizer)
            }
            val playMode by PlayerManager.playMode.collectAsState()
            val playlist by PlayerManager.playlist.collectAsState()
            val mediaLibSongs = AndroidMediaLibRepository.songs
            LaunchedEffect(playMode) {
                SettingRepository.PlayMode = playMode
            }
            LaunchedEffect(mediaLibSongs) {
                playlist?.subtract(mediaLibSongs.toSet())?.forEach {
                    PlayerManager.removeSong(it)
                }
            }
            if (SettingRepository.EnablePlayMemory) {
                val index by PlayerManager.index.collectAsState()
                LaunchedEffect(
                    index,
                    playlist,
                    mediaLibSongs
                ) {
                    withContext(Dispatchers.IO) {
                        SettingRepository.PlayMemory = playlist?.let { it ->
                            PlayMemoryBean(
                                index,
                                it.map { mediaLibSongs.indexOf(it) })
                        }
                    }
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU && SettingRepository.AgreePrivacyPolicy) {
                val permissionState =
                    rememberPermissionState(Manifest.permission.POST_NOTIFICATIONS)
                LaunchedEffect(permissionState.status) {
                    if (permissionState.status is PermissionStatus.Denied) {
                        permissionState.launchPermissionRequest()
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        audioBecomingNoisyManager.setEnabled(false)
    }

}