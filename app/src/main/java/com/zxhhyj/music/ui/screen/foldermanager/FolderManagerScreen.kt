package com.zxhhyj.music.ui.screen.foldermanager

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Folder
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.lifecycleScope
import com.zxhhyj.music.R
import com.zxhhyj.music.logic.config.externalStorageDir
import com.zxhhyj.music.logic.repository.AndroidMediaLibRepository
import com.zxhhyj.music.ui.item.SongItem
import com.zxhhyj.ui.view.AppCenterTopBar
import com.zxhhyj.ui.view.AppScaffold
import com.zxhhyj.ui.view.AppSwitch
import com.zxhhyj.ui.view.item.ItemArrowRight
import com.zxhhyj.ui.view.item.ItemDivider
import com.zxhhyj.ui.view.roundColumn
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable
fun FolderManagerScreen(
    paddingValues: PaddingValues
) {
    AppScaffold(
        topBar = {
            AppCenterTopBar(title = { Text(text = stringResource(id = R.string.folder_manager)) })
        },
        modifier = Modifier.fillMaxSize()
    ) {
        BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
            val lifecycleCoroutineScope = LocalLifecycleOwner.current.lifecycleScope
            val folders = remember {
                (AndroidMediaLibRepository.folders + AndroidMediaLibRepository.hideFolders).sortedBy { it.path }
            }
            LazyColumn(modifier = Modifier.fillMaxWidth(), contentPadding = paddingValues) {
                roundColumn {
                    items(folders) { folder ->
                        var showSongPanel by rememberSaveable {
                            mutableStateOf(false)
                        }
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .animateContentSize()
                        ) {
                            ItemArrowRight(
                                icon = {
                                    Icon(
                                        imageVector = Icons.Rounded.Folder,
                                        contentDescription = null
                                    )
                                },
                                text = {
                                    Text(
                                        text = folder.path.replace(
                                            externalStorageDir.path + "/",
                                            ""
                                        )
                                    )
                                },
                                subText = {
                                    Text(
                                        text = stringResource(
                                            id = R.string.total_n_songs,
                                            folder.songs.size
                                        )
                                    )
                                },
                                actions = {
                                    AppSwitch(checked = AndroidMediaLibRepository.folders.any { it == folder },
                                        onCheckedChange = {
                                            lifecycleCoroutineScope.launch(Dispatchers.IO) {
                                                if (it) {
                                                    AndroidMediaLibRepository.unHideFolder(folder)
                                                } else {
                                                    AndroidMediaLibRepository.hideFolder(folder)
                                                }
                                            }
                                        })
                                }
                            ) {
                                showSongPanel = !showSongPanel
                            }
                            AnimatedVisibility(visible = showSongPanel) {
                                ItemDivider()
                                LazyColumn(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .heightIn(max = this@BoxWithConstraints.maxHeight / 2)
                                ) {
                                    items(folder.songs) {
                                        SongItem(songBean = it)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}