package com.zxhhyj.music.ui.sheet

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.zxhhyj.music.logic.bean.SongBean
import com.zxhhyj.music.ui.item.SongItem
import com.zxhhyj.ui.view.roundColumn

@Composable
fun SongsPreviewSheet(
    songs: List<SongBean>
) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        roundColumn {
            items(songs) {
                SongItem(songBean = it)
            }
        }
    }
}