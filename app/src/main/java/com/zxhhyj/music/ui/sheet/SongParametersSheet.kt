package com.zxhhyj.music.ui.sheet

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.zxhhyj.music.R
import com.zxhhyj.music.logic.bean.SongBean
import com.zxhhyj.music.logic.utils.CopyUtils
import com.zxhhyj.music.logic.utils.timestampToString
import com.zxhhyj.music.logic.utils.toTimeString
import com.zxhhyj.ui.theme.LocalColorScheme
import com.zxhhyj.ui.theme.LocalDimens
import com.zxhhyj.ui.theme.LocalTextStyles
import com.zxhhyj.ui.view.RoundColumn
import com.zxhhyj.ui.view.item.ItemDivider
import com.zxhhyj.ui.view.item.ItemSpacer
import com.zxhhyj.ui.view.item.ItemSubTitle
import com.zxhhyj.ui.view.item.ItemTint

@Composable
fun SongParametersSheet(
    song: SongBean,
) {
    LazyColumn {
        item {
            ItemSpacer()
        }
        item {
            RoundColumn {
                SongParameterItem(
                    title = stringResource(id = R.string.title),
                    info = song.songName
                )
                ItemDivider()
                song.duration?.let {
                    SongParameterItem(
                        title = stringResource(id = R.string.song_duration),
                        info = it.toTimeString()
                    )
                    ItemDivider()
                }
                SongParameterItem(
                    title = stringResource(id = R.string.album),
                    info = song.albumName ?: stringResource(id = R.string.unknown)
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.singer),
                    info = song.artistName ?: stringResource(id = R.string.unknown)
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.bit_rate),
                    info = song.bitrate?.let { "$it kbps" } ?: stringResource(id = R.string.unknown)
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.sample_rate),
                    info = song.samplingRate?.let { "$it Hz" }
                        ?: stringResource(id = R.string.unknown)
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.bit_depth),
                    info = song.bitDepth?.toString() ?: stringResource(id = R.string.unknown)
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.channels),
                    info = "${song.channels}"
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.file_path),
                    info = song.data
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.file_size),
                    info = "${song.size / 1024 / 1024} MB"
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.date_modified),
                    info = timestampToString(song.dateModified)
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.genre),
                    info = song.genre ?: stringResource(id = R.string.unknown)
                )
                ItemDivider()
                SongParameterItem(
                    title = stringResource(id = R.string.encoder),
                    info = song.encoder ?: stringResource(id = R.string.unknown)
                )
            }
        }
        item {
            ItemSubTitle {
                Text(text = stringResource(id = R.string.lyric))
            }
        }
        item {
            RoundColumn {
                ItemTint {
                    Text(text = song.lyric ?: stringResource(id = R.string.unknown))
                }
            }
        }
        item {
            ItemSpacer()
        }
    }
}

@Composable
private fun SongParameterItem(title: String, info: String, modifier: Modifier = Modifier) {
    Row(
        modifier = modifier.padding(
            horizontal = LocalDimens.current.horizontal,
            vertical = LocalDimens.current.vertical
        )
    ) {
        Text(
            text = title,
            color = LocalColorScheme.current.highlight,
            style = LocalTextStyles.current.main
        )
        Spacer(modifier = Modifier.weight(1.0f))
        Text(
            text = info,
            color = LocalColorScheme.current.subText,
            style = LocalTextStyles.current.main,
            textAlign = TextAlign.Right,
            modifier = Modifier
                .padding(start = LocalDimens.current.horizontal)
                .clickable {
                    CopyUtils.copyText(info)
                })
    }
}