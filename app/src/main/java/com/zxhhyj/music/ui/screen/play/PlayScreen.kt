package com.zxhhyj.music.ui.screen.play

import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.displayCutoutPadding
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.FormatListBulleted
import androidx.compose.material.icons.rounded.FontDownload
import androidx.compose.material.icons.rounded.MoreVert
import androidx.compose.material.icons.rounded.Remove
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowHeightSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.mxalbert.sharedelements.FadeMode
import com.mxalbert.sharedelements.MaterialArcMotionFactory
import com.mxalbert.sharedelements.SharedElement
import com.mxalbert.sharedelements.SharedElementsRoot
import com.mxalbert.sharedelements.SharedElementsTransitionSpec
import com.zxhhyj.music.R
import com.zxhhyj.music.logic.repository.SettingRepository
import com.zxhhyj.music.logic.utils.toTimeString
import com.zxhhyj.music.service.playermanager.PlayerManager
import com.zxhhyj.music.ui.common.AlbumMotionBlur
import com.zxhhyj.music.ui.common.AppAsyncImage
import com.zxhhyj.music.ui.common.KeepScreenOn
import com.zxhhyj.music.ui.common.Lyric
import com.zxhhyj.music.ui.common.PanelController
import com.zxhhyj.music.ui.common.PanelState
import com.zxhhyj.music.ui.item.SongItem
import com.zxhhyj.music.ui.SheetDestination
import com.zxhhyj.music.ui.theme.translucentWhiteColor
import com.zxhhyj.music.ui.theme.translucentWhiteFixBugColor
import com.zxhhyj.ui.theme.LocalColorScheme
import com.zxhhyj.ui.theme.LocalDimens
import com.zxhhyj.ui.view.AppCard
import com.zxhhyj.ui.view.AppDivider
import com.zxhhyj.ui.view.AppIconButton
import com.zxhhyj.ui.view.AppSlider
import com.zxhhyj.ui.view.HorizontalNavigationBar
import com.zxhhyj.ui.view.NavigationItem
import com.zxhhyj.ui.view.VerticalNavigationBar
import com.zxhhyj.ui.view.item.ItemSpacer
import dev.olshevski.navigation.reimagined.AnimatedNavHost
import dev.olshevski.navigation.reimagined.AnimatedNavHostScope
import dev.olshevski.navigation.reimagined.NavController
import dev.olshevski.navigation.reimagined.moveToTop
import dev.olshevski.navigation.reimagined.navigate
import dev.olshevski.navigation.reimagined.popUpTo
import dev.olshevski.navigation.reimagined.rememberNavController

private enum class PlayScreenDestination {
    Controller, Lyric, PlayQueue,
}

private val PlayScreenDestination.tabIcon
    get() = when (this) {
        PlayScreenDestination.Controller -> throw RuntimeException()
        PlayScreenDestination.Lyric -> Icons.Rounded.FontDownload
        PlayScreenDestination.PlayQueue -> Icons.AutoMirrored.Rounded.FormatListBulleted
    }

private const val ShareAlbumKey = "album"

private val AnimDurationMillis
    get() = if (!SettingRepository.EnableLinkUI) 300 else 0

private object PlayScreen {
    val PlayScreenContentHorizontal
        @Composable get() = LocalDimens.current.horizontal
    val PlayScreenHorizontal = 12.dp
}

private val MaterialFadeInTransitionSpec
    get() = SharedElementsTransitionSpec(
        pathMotionFactory = MaterialArcMotionFactory,
        durationMillis = AnimDurationMillis,
        fadeMode = FadeMode.In
    )

private val MaterialFadeOutTransitionSpec
    get() = SharedElementsTransitionSpec(
        pathMotionFactory = MaterialArcMotionFactory,
        durationMillis = AnimDurationMillis,
        fadeMode = FadeMode.Out
    )

@OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
@Composable
fun PlayScreen(
    sheetNavController: NavController<SheetDestination>, panelController: PanelController
) {

    val windowSizeClass = calculateWindowSizeClass(LocalContext.current as ComponentActivity)

    val navController = rememberNavController(startDestination = PlayScreenDestination.Controller)

    val lastDestination = navController.backstack.entries.last().destination

    Box(
        modifier = Modifier
            .fillMaxSize()
            .clip(
                shape = RoundedCornerShape(
                    topStart = LocalDimens.current.round,
                    topEnd = LocalDimens.current.round
                )
            )
    ) {
        SharedElementsRoot {
            when (SettingRepository.EnableLinkUI) {
                true -> {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Color.DarkGray)
                    )
                }

                false -> {
                    val currentSong by PlayerManager.currentSong.collectAsState()
                    AlbumMotionBlur(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Color.DarkGray),
                        albumUrl = currentSong?.coverUrl,
                        paused = panelController.panelState == PanelState.COLLAPSED
                    )
                }
            }

            BackHandler(panelController.panelState == PanelState.EXPANDED) {
                panelController.swipeTo(PanelState.COLLAPSED)
            }
            when (windowSizeClass.widthSizeClass) {
                WindowWidthSizeClass.Compact -> {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .systemBarsPadding()
                            .padding(horizontal = PlayScreen.PlayScreenHorizontal),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxSize()
                                .weight(1.0f),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            BackHandler(lastDestination != PlayScreenDestination.Controller) {
                                navController.popUpTo { it == PlayScreenDestination.Controller }
                            }
                            BaseAnimatedNavHost(navController = navController) {
                                when (it) {
                                    PlayScreenDestination.Controller -> {
                                        when (windowSizeClass.heightSizeClass) {
                                            WindowHeightSizeClass.Compact -> {
                                                Column(
                                                    modifier = Modifier
                                                        .fillMaxSize()
                                                        .padding(horizontal = PlayScreen.PlayScreenContentHorizontal),
                                                    verticalArrangement = Arrangement.Center,
                                                    horizontalAlignment = Alignment.CenterHorizontally
                                                ) {
                                                    SongNameAndSingerName(
                                                        modifier = Modifier.fillMaxWidth(),
                                                        sheetNavController = sheetNavController
                                                    )

                                                    ItemSpacer()

                                                    ProgressBarAndTitle(modifier = Modifier.fillMaxWidth())

                                                    ControlButtons(modifier = Modifier.fillMaxWidth())
                                                }
                                            }

                                            WindowHeightSizeClass.Medium, WindowHeightSizeClass.Expanded -> {
                                                Column(
                                                    modifier = Modifier
                                                        .fillMaxSize()
                                                        .padding(horizontal = PlayScreen.PlayScreenContentHorizontal),
                                                    verticalArrangement = Arrangement.Bottom,
                                                    horizontalAlignment = Alignment.CenterHorizontally
                                                ) {

                                                    AlbumCover(
                                                        modifier = Modifier
                                                            .aspectRatio(
                                                                ratio = 1f,
                                                                matchHeightConstraintsFirst = true
                                                            )
                                                            .fillMaxSize()
                                                    )

                                                    SongNameAndSingerName(
                                                        modifier = Modifier.fillMaxWidth(),
                                                        sheetNavController = sheetNavController
                                                    )

                                                    ItemSpacer()

                                                    ProgressBarAndTitle(modifier = Modifier.fillMaxWidth())

                                                    ControlButtons(
                                                        modifier = Modifier
                                                            .fillMaxWidth()
                                                            .height(180.dp)
                                                    )
                                                }
                                            }
                                        }

                                    }

                                    PlayScreenDestination.Lyric -> {
                                        Column(modifier = Modifier.fillMaxSize()) {
                                            LazyColumn(userScrollEnabled = false) {
                                                item {
                                                    TopSongItem(
                                                        navController = navController,
                                                        sheetNavController = sheetNavController
                                                    )
                                                }
                                            }
                                            LyricScreen()
                                        }
                                    }

                                    PlayScreenDestination.PlayQueue -> {
                                        Column(modifier = Modifier.fillMaxSize()) {
                                            LazyColumn(userScrollEnabled = false) {
                                                item {
                                                    TopSongItem(
                                                        navController = navController,
                                                        sheetNavController = sheetNavController
                                                    )
                                                }
                                            }
                                            QueueScreen(
                                                panelController = panelController
                                            )
                                        }
                                    }
                                }
                            }
                        }
                        HorizontalNavigationBarInset(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = PlayScreen.PlayScreenContentHorizontal),
                            navController = navController
                        ) { selected, screen ->
                            if (selected) {
                                navController.popUpTo {
                                    it == PlayScreenDestination.Controller
                                }
                                return@HorizontalNavigationBarInset
                            }
                            if (!navController.moveToTop { it == screen }) {
                                navController.navigate(screen)
                            }
                        }
                    }
                }

                WindowWidthSizeClass.Medium, WindowWidthSizeClass.Expanded -> {
                    when (windowSizeClass.heightSizeClass) {
                        WindowHeightSizeClass.Compact -> {
                            Row(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .systemBarsPadding()
                                    .displayCutoutPadding()
                            ) {
                                VerticalNavigationBarInset(
                                    modifier = Modifier.fillMaxHeight(),
                                    navController = navController
                                ) { selected, screen ->
                                    if (selected) {
                                        navController.popUpTo {
                                            it == PlayScreenDestination.Controller
                                        }
                                        return@VerticalNavigationBarInset
                                    }
                                    if (!navController.moveToTop { it == screen }) {
                                        navController.navigate(screen)
                                    }
                                }
                                Box(
                                    modifier = Modifier
                                        .fillMaxHeight()
                                        .weight(1.0f)
                                ) {
                                    BaseAnimatedNavHost(navController = navController) {
                                        when (it) {
                                            PlayScreenDestination.Controller -> {
                                                Row(
                                                    modifier = Modifier
                                                        .fillMaxSize()
                                                        .padding(horizontal = PlayScreen.PlayScreenContentHorizontal),
                                                ) {
                                                    Box(
                                                        modifier = Modifier
                                                            .fillMaxSize()
                                                            .weight(1.0f),
                                                        contentAlignment = Alignment.Center
                                                    ) {
                                                        AlbumCover(
                                                            modifier = Modifier
                                                                .align(
                                                                    Alignment.Center
                                                                )
                                                                .aspectRatio(
                                                                    ratio = 1f,
                                                                    matchHeightConstraintsFirst = true
                                                                )
                                                                .fillMaxSize()
                                                        )
                                                    }
                                                    Column(
                                                        modifier = Modifier
                                                            .fillMaxSize()
                                                            .weight(1.0f),
                                                        verticalArrangement = Arrangement.Center
                                                    ) {
                                                        SongNameAndSingerName(
                                                            modifier = Modifier.fillMaxWidth(),
                                                            sheetNavController = sheetNavController
                                                        )

                                                        ItemSpacer()

                                                        ProgressBarAndTitle(modifier = Modifier.fillMaxWidth())

                                                        ControlButtons(modifier = Modifier.fillMaxWidth())
                                                    }
                                                }
                                            }

                                            PlayScreenDestination.Lyric, PlayScreenDestination.PlayQueue -> {
                                                Column(modifier = Modifier.fillMaxSize()) {
                                                    LazyColumn(userScrollEnabled = false) {
                                                        item {
                                                            TopSongItem(
                                                                navController = navController,
                                                                sheetNavController = sheetNavController
                                                            )
                                                        }
                                                    }
                                                    when (it) {
                                                        PlayScreenDestination.Lyric -> LyricScreen()
                                                        PlayScreenDestination.PlayQueue -> QueueScreen(
                                                            panelController = panelController
                                                        )

                                                        else -> {}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        WindowHeightSizeClass.Medium, WindowHeightSizeClass.Expanded -> {
                            Row(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .systemBarsPadding()
                                    .displayCutoutPadding()
                                    .padding(horizontal = PlayScreen.PlayScreenHorizontal),
                                horizontalArrangement = Arrangement.SpaceAround
                            ) {
                                Column(
                                    modifier = Modifier
                                        .fillMaxHeight()
                                        .weight(1.0f)
                                        .padding(horizontal = PlayScreen.PlayScreenHorizontal),
                                    horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Column(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .weight(1.0f),
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        verticalArrangement = Arrangement.Center
                                    ) {
                                        AlbumCover(
                                            modifier = Modifier
                                                .aspectRatio(
                                                    ratio = 1f,
                                                    matchHeightConstraintsFirst = true
                                                )
                                                .fillMaxSize()
                                        )
                                        SongNameAndSingerName(
                                            modifier = Modifier.fillMaxWidth(),
                                            sheetNavController = sheetNavController
                                        )
                                        ProgressBarAndTitle(modifier = Modifier.fillMaxWidth())
                                        ControlButtons(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .height(200.dp)
                                        )
                                    }
                                    HorizontalNavigationBarInset(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(horizontal = PlayScreen.PlayScreenContentHorizontal),
                                        navController = navController
                                    ) { _, screen ->
                                        if (!navController.moveToTop { it == screen }) {
                                            navController.navigate(screen)
                                        }
                                    }
                                }
                                Box(
                                    modifier = Modifier
                                        .fillMaxHeight()
                                        .weight(1.0f)
                                        .padding(horizontal = PlayScreen.PlayScreenHorizontal)
                                ) {
                                    BaseAnimatedNavHost(navController = navController) {
                                        when (it) {
                                            PlayScreenDestination.Lyric, PlayScreenDestination.PlayQueue -> {
                                                Column(modifier = Modifier.fillMaxSize()) {
                                                    when (it) {
                                                        PlayScreenDestination.Lyric -> LyricScreen()
                                                        PlayScreenDestination.PlayQueue -> QueueScreen(
                                                            panelController = panelController
                                                        )

                                                        else -> {}
                                                    }
                                                }
                                            }

                                            else -> {}
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun LyricScreen() {
    val pause by PlayerManager.pause.collectAsState()
    KeepScreenOn(enable = !pause)
    val currentSong by PlayerManager.currentSong.collectAsState()
    val liveTime by PlayerManager.progress.collectAsState()
    Lyric(
        modifier = Modifier.fillMaxSize(),
        lyricItemModifier = Modifier.padding(
            horizontal = PlayScreen.PlayScreenContentHorizontal,
            vertical = LocalDimens.current.vertical
        ),
        lyric = currentSong?.lyric,
        liveTime = liveTime,
        translation = SettingRepository.EnableLyricsTranslation
    ) {
        PlayerManager.seekTo(it)
        PlayerManager.start()
    }
}

@Composable
private fun QueueScreen(panelController: PanelController) {
    val playlist by PlayerManager.playlist.collectAsState()
    val currentSong by PlayerManager.currentSong.collectAsState()
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = PlayScreen.PlayScreenContentHorizontal,
                vertical = LocalDimens.current.vertical
            ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column {
            Text(
                text = stringResource(id = R.string.play_list),
                color = Color.White,
                fontSize = 16.sp,
                textAlign = TextAlign.Center
            )
            Text(
                text = "${(playlist?.indexOf(currentSong) ?: 0) + 1}/${playlist?.size}",
                color = translucentWhiteColor,
                fontSize = 12.sp
            )
        }
        Spacer(modifier = Modifier.weight(1.0f))
        Text(text = stringResource(id = R.string.clear),
            color = translucentWhiteColor,
            fontSize = 14.sp,
            modifier = Modifier.clickable {
                panelController.swipeTo(PanelState.COLLAPSED)
                PlayerManager.clearPlayList()
            })
    }

    AppDivider(
        color = translucentWhiteColor,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = PlayScreen.PlayScreenContentHorizontal)
    )

    BoxWithConstraints(
        modifier = Modifier
            .fillMaxSize()
    ) {

        val lazyListState = rememberLazyListState()

        var selectItemBoxSize by remember {
            mutableStateOf(IntSize.Zero)
        }

        val currentIndex by PlayerManager.index.collectAsState()

        CompositionLocalProvider(
            LocalColorScheme provides LocalColorScheme.current.copy(
                text = Color.White, subText = translucentWhiteColor
            ), LocalContentColor provides translucentWhiteColor
        ) {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                state = lazyListState,
                contentPadding = PaddingValues(top = LocalDimens.current.vertical)
            ) {
                playlist?.let { songBeans ->
                    itemsIndexed(songBeans) { index, model ->
                        AppCard(backgroundColor = if (currentIndex == index) Color.White.copy(alpha = 0.2f) else Color.Transparent,
                            modifier = Modifier
                                .onSizeChanged {
                                    selectItemBoxSize = it
                                }
                                .fillMaxWidth()) {
                            SongItem(songBean = model, actions = {
                                AppIconButton(onClick = {
                                    PlayerManager.removeSong(model)
                                }) {
                                    Icon(
                                        imageVector = Icons.Rounded.Remove,
                                        contentDescription = null,
                                    )
                                }
                            }) { PlayerManager.play(songBeans, index) }
                        }
                    }
                }
            }
        }
        val boxHeightPx = with(LocalDensity.current) {
            maxHeight.roundToPx()
        }
        LaunchedEffect(Unit) {
            val height = (boxHeightPx - selectItemBoxSize.height) / 2
            lazyListState.scrollToItem(currentIndex.coerceAtLeast(0), -height)
        }
        LaunchedEffect(currentIndex) {
            val height = (boxHeightPx - selectItemBoxSize.height) / 2
            lazyListState.animateScrollToItem(currentIndex.coerceAtLeast(0), -height)
        }
    }
}

@Composable
private fun TopSongItem(
    modifier: Modifier = Modifier,
    navController: NavController<PlayScreenDestination>,
    sheetNavController: NavController<SheetDestination>,
) {
    val albumHeight = 56.dp
    val currentSong by PlayerManager.currentSong.collectAsState()
    Row(
        modifier = modifier.padding(horizontal = PlayScreen.PlayScreenContentHorizontal),
        verticalAlignment = Alignment.CenterVertically
    ) {
        SharedElement(
            key = ShareAlbumKey,
            screenKey = "top_song_item",
            transitionSpec = MaterialFadeInTransitionSpec
        ) {
            AppAsyncImage(
                modifier = Modifier
                    .padding(
                        PaddingValues.Absolute(
                            left = 0.dp,
                            right = LocalDimens.current.horizontal / 2,
                            top = LocalDimens.current.vertical,
                            bottom = LocalDimens.current.vertical
                        )
                    )
                    .size(albumHeight), data = currentSong?.coverUrl
            ) {
                navController.moveToTop {
                    it == PlayScreenDestination.Controller
                }
            }
        }
        Column(
            modifier = Modifier
                .weight(1.0f)
                .height(albumHeight),
            verticalArrangement = Arrangement.SpaceAround,
        ) {
            currentSong?.songName?.let {
                Text(
                    text = it,
                    color = Color.White,
                    fontSize = 16.sp,
                    maxLines = 1,
                    textAlign = TextAlign.Center,
                    overflow = TextOverflow.Ellipsis
                )
            }
            currentSong?.artistName?.let {
                Text(
                    text = it,
                    color = translucentWhiteColor,
                    fontSize = 14.sp,
                    maxLines = 1,
                    textAlign = TextAlign.Center,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
        Icon(
            imageVector = Icons.Rounded.MoreVert,
            tint = Color.White,
            contentDescription = null,
            modifier = Modifier
                .size(32.dp)
                .clip(RoundedCornerShape(50))
                .background(translucentWhiteFixBugColor)
                .clickable {
                    currentSong?.let {
                        sheetNavController.navigate(SheetDestination.SongPanel(it))
                    }
                })
    }
}


@Composable
private fun ControlButtons(modifier: Modifier) {
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        CompositionLocalProvider(
            LocalColorScheme provides LocalColorScheme.current.copy(disabled = translucentWhiteColor),
            LocalContentColor provides Color.White
        ) {
            val buttonSize = 64.dp
            val index by PlayerManager.index.collectAsState()
            val playlist by PlayerManager.playlist.collectAsState()

            AppIconButton(
                onClick = { PlayerManager.skipToPrevious() },
                enabled = index > 0,
                modifier = Modifier.size(buttonSize)
            ) {
                Icon(
                    imageVector = ImageVector.vectorResource(id = R.drawable.ic_skip_previous),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxSize()
                        .scale(0.8f)
                )
            }

            val pauseState by PlayerManager.pause.collectAsState()
            val playPauseIcon = if (pauseState) R.drawable.ic_play else R.drawable.ic_pause

            AppIconButton(
                onClick = {
                    if (pauseState) {
                        PlayerManager.start()
                    } else {
                        PlayerManager.pause()
                    }
                }, modifier = Modifier
                    .padding(horizontal = 20.dp)
                    .size(buttonSize)
                    .scale(1.2f)
            ) {
                Icon(
                    imageVector = ImageVector.vectorResource(id = playPauseIcon),
                    contentDescription = null,
                    modifier = Modifier.fillMaxSize()
                )
            }

            AppIconButton(
                onClick = { PlayerManager.skipToNext() },
                enabled = index != playlist?.size?.let { it - 1 },
                modifier = Modifier.size(buttonSize)
            ) {
                Icon(
                    imageVector = ImageVector.vectorResource(id = R.drawable.ic_skip_next),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxSize()
                        .scale(0.8f)
                )
            }
        }

    }
}

@Composable
private fun ProgressBarAndTitle(modifier: Modifier) {
    val progress by PlayerManager.progress.collectAsState()

    val duration by PlayerManager.duration.collectAsState()

    val sliderHeight = 16.dp

    AppSlider(
        value = progress.toFloat(),
        valueRange = 0f..duration.toFloat(),
        onValueChange = {
            PlayerManager.seekTo(it.toInt())
        },
        onDragStart = {
            PlayerManager.pause()
        },
        onDragEnd = {
            PlayerManager.start()
        },
        thumbSize = sliderHeight,
        modifier = modifier
            .height(sliderHeight),
        thumbColor = Color.White,
        activeTrackColor = Color.White,
        inactiveTrackColor = translucentWhiteFixBugColor,
    )

    Row(
        modifier = modifier
    ) {
        Text(
            text = progress.toTimeString(), color = translucentWhiteColor, fontSize = 14.sp
        )
        Spacer(modifier = Modifier.weight(1f))
        Text(
            text = duration.toTimeString(), color = translucentWhiteColor, fontSize = 14.sp
        )
    }
}

@Composable
private fun AlbumCover(modifier: Modifier) {
    val currentSong by PlayerManager.currentSong.collectAsState()
    SharedElement(
        key = ShareAlbumKey,
        screenKey = "album",
        transitionSpec = MaterialFadeOutTransitionSpec
    ) {
        val elevation = LocalDimens.current.vertical
        AppCard(
            modifier = Modifier
                .then(modifier)
                .padding(bottom = elevation),
            backgroundColor = Color.DarkGray,
            elevation = elevation,
        ) {
            AppAsyncImage(
                modifier = Modifier.fillMaxSize(), data = currentSong?.coverUrl
            )
        }
    }
}

@Composable
private fun SongNameAndSingerName(
    modifier: Modifier,
    sheetNavController: NavController<SheetDestination>
) {
    val currentSong by PlayerManager.currentSong.collectAsState()
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column(
            modifier = Modifier.weight(1.0f),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = currentSong?.songName ?: "",
                color = Color.White,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1
            )
            Text(
                text = currentSong?.artistName ?: "",
                color = translucentWhiteColor,
                fontSize = 16.sp,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1
            )
        }
        Icon(
            imageVector = Icons.Rounded.MoreVert,
            tint = Color.White,
            contentDescription = null,
            modifier = Modifier
                .size(32.dp)
                .clip(RoundedCornerShape(50))
                .background(translucentWhiteFixBugColor)
                .clickable {
                    currentSong?.let {
                        sheetNavController.navigate(SheetDestination.SongPanel(it))
                    }
                })
    }
}

@Composable
private fun BaseAnimatedNavHost(
    navController: NavController<PlayScreenDestination>,
    contentSelector: @Composable AnimatedNavHostScope<PlayScreenDestination>.(destination: PlayScreenDestination) -> Unit
) {
    AnimatedNavHost(
        controller = navController,
        transitionSpec = { _, _, _ ->
            if (SettingRepository.EnableLinkUI) {
                EnterTransition.None togetherWith ExitTransition.None
            } else {
                val tween =
                    tween<Float>(durationMillis = AnimDurationMillis)
                fadeIn(tween) togetherWith fadeOut(tween)
            }
        },
        contentSelector = contentSelector
    )
}

@Composable
private fun HorizontalNavigationBarInset(
    modifier: Modifier,
    navController: NavController<PlayScreenDestination>,
    onClick: (selected: Boolean, screen: PlayScreenDestination) -> Unit
) {
    val lastDestination = navController.backstack.entries.last().destination
    HorizontalNavigationBar(
        modifier = modifier,
        backgroundColor = Color.Transparent
    ) {
        listOf(
            PlayScreenDestination.Lyric, PlayScreenDestination.PlayQueue
        ).forEach { screen ->
            val selected = screen == lastDestination
            NavigationItem(
                modifier = Modifier.clip(RoundedCornerShape(LocalDimens.current.round)),
                icon = { Icon(screen.tabIcon, contentDescription = screen.name) },
                selectedContentColor = Color.White,
                unselectedContentColor = translucentWhiteColor,
                selected = selected,
                onClick = {
                    onClick.invoke(selected, screen)
                })
        }
    }
}

@Composable
private fun VerticalNavigationBarInset(
    modifier: Modifier,
    navController: NavController<PlayScreenDestination>,
    onClick: (selected: Boolean, screen: PlayScreenDestination) -> Unit
) {
    val lastDestination = navController.backstack.entries.last().destination
    VerticalNavigationBar(
        modifier = modifier,
        backgroundColor = Color.Transparent
    ) {
        listOf(
            PlayScreenDestination.Lyric, PlayScreenDestination.PlayQueue
        ).forEach { screen ->
            val selected = screen == lastDestination
            NavigationItem(
                modifier = Modifier
                    .clip(RoundedCornerShape(LocalDimens.current.round))
                    .weight(1.0f),
                selected = selected,
                onClick = {
                    onClick.invoke(selected, screen)
                },
                icon = { Icon(screen.tabIcon, contentDescription = screen.name) },
                selectedContentColor = Color.White,
                unselectedContentColor = translucentWhiteColor
            )
        }
    }
}