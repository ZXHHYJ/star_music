package com.zxhhyj.music.ui.common

import androidx.compose.foundation.Canvas
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun PieChart(modifier: Modifier = Modifier, vararg args: Pair<Color, Float>) {
    if (args.map { it.second }.fold(0f) { acc, i -> acc + i } > 100f) {
        throw IllegalArgumentException("The second argument exceeds 100f")
    }
    Canvas(modifier = modifier) {
//        drawArc(
//            color = Color.Cyan,
//            startAngle = 90F,
//            sweepAngle = 120F,
//            useCenter = true,
//            topLeft = Offset(x = 160.dp.toPx(), y = 200.dp.toPx()),
//            size = DpSize(100.dp, 100.dp).toSize(),
//        )
    }
}