package com.zxhhyj.music.ui.sheet

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.AddToQueue
import androidx.compose.material.icons.rounded.Album
import androidx.compose.material.icons.rounded.Delete
import androidx.compose.material.icons.rounded.HideSource
import androidx.compose.material.icons.rounded.Info
import androidx.compose.material.icons.rounded.Person
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import com.zxhhyj.music.R
import com.zxhhyj.music.logic.bean.SongBean
import com.zxhhyj.music.logic.repository.AndroidMediaLibRepository
import com.zxhhyj.music.service.playermanager.PlayerManager
import com.zxhhyj.music.ui.common.ComposeToast
import com.zxhhyj.music.ui.item.SongItem
import com.zxhhyj.music.ui.DialogDestination
import com.zxhhyj.music.ui.ScreenDestination
import com.zxhhyj.music.ui.SheetDestination
import com.zxhhyj.ui.view.RoundColumn
import com.zxhhyj.ui.view.item.Item
import com.zxhhyj.ui.view.item.ItemArrowRight
import com.zxhhyj.ui.view.item.ItemDivider
import com.zxhhyj.ui.view.item.ItemSpacer
import dev.olshevski.navigation.reimagined.NavController
import dev.olshevski.navigation.reimagined.navigate
import dev.olshevski.navigation.reimagined.popAll

@Composable
fun SongMenuSheet(
    mainNavController: NavController<ScreenDestination>,
    sheetNavController: NavController<SheetDestination>,
    dialogNavController: NavController<DialogDestination>,
    songBean: SongBean,
) {
    val context = LocalContext.current
    val currentSong by PlayerManager.currentSong.collectAsState()
    LazyColumn {
        item {
            ItemSpacer()
        }
        item {
            RoundColumn {
                SongItem(songBean = songBean)
            }
        }
        item {
            ItemSpacer()
        }
        item {
            RoundColumn {
                ItemArrowRight(
                    icon = { Icon(imageVector = Icons.Rounded.Album, contentDescription = null) },
                    text = {
                        Text(
                            text = songBean.albumName
                                ?: stringResource(id = R.string.unknown)
                        )
                    },
                    subText = { Text(text = stringResource(id = R.string.album)) },
                    enabled = songBean.albumName != null
                ) {
                    songBean.albumName?.let {
                        sheetNavController.popAll()
                        mainNavController.navigate(ScreenDestination.AlbumCnt(it))
                    }
                }
                ItemDivider()
                ItemArrowRight(
                    icon = { Icon(imageVector = Icons.Rounded.Person, contentDescription = null) },
                    text = {
                        Text(
                            text = songBean.artistName
                                ?: stringResource(id = R.string.unknown)
                        )
                    },
                    subText = { Text(text = stringResource(id = R.string.singer)) },
                    enabled = songBean.artistName != null
                ) {
                    songBean.artistName?.let {
                        sheetNavController.popAll()
                        mainNavController.navigate(ScreenDestination.SingerCnt(it))
                    }
                }
            }
        }
        item {
            ItemSpacer()
        }
        item {
            RoundColumn {
                ItemArrowRight(
                    icon = { Icon(imageVector = Icons.Rounded.Add, contentDescription = null) },
                    text = { Text(text = stringResource(id = R.string.add_to_playlist)) },
                    subText = { }) {
                    sheetNavController.navigate(SheetDestination.AddToPlayList(songBean))
                }
                ItemDivider()
                Item(
                    icon = {
                        Icon(
                            imageVector = Icons.Rounded.AddToQueue,
                            contentDescription = null
                        )
                    },
                    text = { Text(text = stringResource(id = R.string.next_player)) },
                    subText = { },
                    enabled = currentSong != null
                ) {
                    sheetNavController.popAll()
                    PlayerManager.addNextPlay(songBean)
                    ComposeToast.postToast(context.getString(R.string.added_to_playlist))
                }
                ItemDivider()
                ItemArrowRight(
                    icon = { Icon(imageVector = Icons.Rounded.Info, contentDescription = null) },
                    text = {
                        Text(
                            text = stringResource(id = R.string.parameters_info)
                        )
                    },
                    subText = { }) {
                    sheetNavController.navigate(SheetDestination.SongParameters(songBean))
                }
            }
        }
        item {
            ItemSpacer()
        }
        item {
            RoundColumn {
                Item(
                    icon = {
                        Icon(
                            imageVector = Icons.Rounded.HideSource,
                            contentDescription = null
                        )
                    },
                    text = {
                        Text(
                            text = stringResource(id = R.string.hide)
                        )
                    },
                    subText = { }
                ) {
                    sheetNavController.popAll()
                    AndroidMediaLibRepository.hideSong(songBean)
                }
                ItemDivider()
                Item(
                    icon = {
                        Icon(
                            imageVector = Icons.Rounded.Delete,
                            contentDescription = null
                        )
                    },
                    text = {
                        Text(
                            text = stringResource(id = R.string.delete)
                        )
                    },
                    subText = { }
                ) {
                    dialogNavController.navigate(DialogDestination.DeleteSong(songBean))
                }
            }
        }
    }
}
