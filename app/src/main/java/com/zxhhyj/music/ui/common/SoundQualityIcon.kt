package com.zxhhyj.music.ui.common

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.zxhhyj.music.logic.bean.SongBean

private val fontSize = 8.sp
private val shape = RoundedCornerShape(2.dp)

@Composable
fun SoundQualityIcon(modifier: Modifier = Modifier, song: SongBean) {
    when {
        (song.bitrate != null && song.bitrate >= 960 && song.bitDepth != null && song.bitDepth >= 24) -> {
            Text(
                text = "HR",
                modifier = modifier
                    .background(Color.Yellow.copy(0.5f), shape)
                    .padding(horizontal = 2.dp),
                fontSize = fontSize,
                textAlign = TextAlign.Center
            )
        }

        (song.samplingRate != null && song.samplingRate >= 44100 && song.bitDepth != null && song.bitDepth >= 16) -> {
            Text(
                text = "SQ",
                modifier = modifier
                    .background(Color.Red.copy(0.1f), shape)
                    .padding(horizontal = 2.dp),
                fontSize = fontSize,
                textAlign = TextAlign.Center
            )
        }

        song.bitrate != null && song.bitrate >= 128 -> {
            Text(
                text = "HQ",
                modifier = modifier
                    .background(Color.Blue.copy(0.1f), shape)
                    .padding(horizontal = 2.dp),
                fontSize = fontSize,
                textAlign = TextAlign.Center
            )
        }

        else -> {
            //nothing
        }
    }
}