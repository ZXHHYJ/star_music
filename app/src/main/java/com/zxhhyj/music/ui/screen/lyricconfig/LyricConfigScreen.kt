package com.zxhhyj.music.ui.screen.lyricconfig

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.FormatBold
import androidx.compose.material.icons.rounded.Translate
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.zxhhyj.music.R
import com.zxhhyj.music.logic.repository.SettingRepository
import com.zxhhyj.music.service.playermanager.PlayerManager
import com.zxhhyj.music.ui.common.AlbumMotionBlur
import com.zxhhyj.music.ui.common.Lyric
import com.zxhhyj.ui.theme.LocalDimens
import com.zxhhyj.ui.view.AppCenterTopBar
import com.zxhhyj.ui.view.AppScaffold
import com.zxhhyj.ui.view.AppTab
import com.zxhhyj.ui.view.AppTabRow
import com.zxhhyj.ui.view.RoundColumn
import com.zxhhyj.ui.view.item.ItemDivider
import com.zxhhyj.ui.view.item.ItemSlider
import com.zxhhyj.ui.view.item.ItemSwitcher

enum class LyricConfigTabs {
    DEFAULT, NOW_PLAYING
}

val LyricConfigTabs.tabName: String
    @Composable get() = when (this) {
        LyricConfigTabs.DEFAULT -> stringResource(id = R.string.default_)
        LyricConfigTabs.NOW_PLAYING -> stringResource(id = R.string.now_playing)
    }

private const val defaultLyric =
    "[00:00.00]我着眼\n[00:00.01]万万千渺小、偏执、浪漫的方圆\n[00:00.00]我著眼\n[00:00.01]萬萬千渺小、偏執、浪漫的方圓"

@Composable
fun LyricConfigScreen(paddingValues: PaddingValues) {
    AppScaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            AppCenterTopBar(title = { Text(text = stringResource(id = R.string.lyric)) })
        }) {
        var currentLyricShowType by rememberSaveable {
            mutableStateOf(LyricConfigTabs.DEFAULT)
        }
        LazyColumn(
            modifier = Modifier
                .fillMaxSize(),
            contentPadding = paddingValues
        ) {
            item {
                RoundColumn {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .heightIn(max = 300.dp)
                    ) {
                        val currentSong by PlayerManager.currentSong.collectAsState()
                        AlbumMotionBlur(
                            modifier = Modifier
                                .fillMaxSize()
                                .background(Color.DarkGray),
                            albumUrl = currentSong?.coverUrl,
                            paused = false
                        )
                        when (currentLyricShowType) {
                            LyricConfigTabs.DEFAULT -> {
                                Lyric(
                                    modifier = Modifier
                                        .fillMaxSize(),
                                    lyricItemModifier = Modifier.padding(
                                        horizontal = LocalDimens.current.horizontal,
                                        vertical = LocalDimens.current.vertical
                                    ),
                                    lyric = defaultLyric,
                                    liveTime = 10,
                                    translation = SettingRepository.EnableLyricsTranslation
                                ) {}
                            }

                            LyricConfigTabs.NOW_PLAYING -> {
                                //这里参数与播放界面的歌词要保持一致
                                val liveTime by PlayerManager.progress.collectAsState()
                                Lyric(
                                    modifier = Modifier
                                        .fillMaxSize(),
                                    lyricItemModifier = Modifier.padding(
                                        horizontal = LocalDimens.current.horizontal,
                                        vertical = LocalDimens.current.vertical
                                    ),
                                    lyric = currentSong?.lyric,
                                    liveTime = liveTime,
                                    translation = SettingRepository.EnableLyricsTranslation
                                ) {}
                            }
                        }

                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .pointerInput(Unit) {
                                    detectTapGestures { /* 处理点击事件 */ }
                                    detectDragGestures { _, _ -> /* 处理拖动事件 */ }
                                }
                        ) {
                            // 空内容，用于拦截触摸事件
                        }
                    }
                }
            }
            item {
                AppTabRow {
                    LyricConfigTabs.entries.forEach {
                        AppTab(
                            selected = currentLyricShowType == it,
                            onClick = { currentLyricShowType = it }) {
                            Text(text = it.tabName)
                        }
                    }
                }
            }
            item {
                val valueRange = 20f..44f
                RoundColumn {
                    ItemSlider(
                        text = {
                            Text(text = stringResource(id = R.string.lyric_font_size))
                        },
                        subText = {
                            Text(text = "${SettingRepository.LyricFontSize}")
                        },
                        value = SettingRepository.LyricFontSize.toFloat(),
                        onValueChange = {
                            SettingRepository.LyricFontSize = it.toInt()
                        },
                        valueRange = valueRange
                    )
                    ItemDivider()
                    ItemSlider(
                        text = {
                            Text(text = stringResource(id = R.string.lyric_lyric_font_line_height))
                        },
                        subText = {
                            Text(text = "${SettingRepository.LyricFontLineHeight}")
                        },
                        value = SettingRepository.LyricFontLineHeight.toFloat(),
                        onValueChange = {
                            SettingRepository.LyricFontLineHeight = it.toInt()
                        },
                        valueRange = (valueRange.start + 2f)..(valueRange.endInclusive + 2f)
                    )
                    ItemDivider()
                    ItemSwitcher(
                        icon = {
                            Icon(
                                imageVector = Icons.Rounded.FormatBold,
                                contentDescription = null
                            )
                        },
                        text = { Text(text = stringResource(id = R.string.lyric_font_bold)) },
                        subText = { },
                        checked = SettingRepository.LyricFontBold,
                        onCheckedChange = {
                            SettingRepository.LyricFontBold = it
                        }
                    )
                    ItemDivider()
                    ItemSwitcher(
                        icon = {
                            Icon(
                                imageVector = Icons.Rounded.Translate,
                                contentDescription = null
                            )
                        },
                        text = {
                            Text(text = stringResource(id = R.string.lyrics_translation))
                        },
                        subText = {},
                        checked = SettingRepository.EnableLyricsTranslation,
                        onCheckedChange = {
                            SettingRepository.EnableLyricsTranslation = it
                        }
                    )
                }
            }
        }
    }
}
