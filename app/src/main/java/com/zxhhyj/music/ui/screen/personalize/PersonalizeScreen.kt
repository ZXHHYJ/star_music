package com.zxhhyj.music.ui.screen.personalize

import android.os.Build
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.Label
import androidx.compose.material.icons.rounded.BorderStyle
import androidx.compose.material.icons.rounded.Lyrics
import androidx.compose.material.icons.rounded.Palette
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.zxhhyj.music.R
import com.zxhhyj.music.logic.config.nothingSong
import com.zxhhyj.music.logic.repository.AndroidMediaLibRepository
import com.zxhhyj.music.logic.repository.SettingRepository
import com.zxhhyj.music.service.playermanager.PlayerManager
import com.zxhhyj.music.ui.item.SongItem
import com.zxhhyj.music.ui.ScreenDestination
import com.zxhhyj.music.ui.SheetDestination
import com.zxhhyj.ui.view.AppCenterTopBar
import com.zxhhyj.ui.view.AppScaffold
import com.zxhhyj.ui.view.RoundColumn
import com.zxhhyj.ui.view.item.ItemArrowRight
import com.zxhhyj.ui.view.item.ItemDivider
import com.zxhhyj.ui.view.item.ItemSpacer
import com.zxhhyj.ui.view.item.ItemSwitcher
import dev.olshevski.navigation.reimagined.NavController
import dev.olshevski.navigation.reimagined.navigate
import dev.olshevski.navigation.reimagined.rememberNavController

@Composable
fun PersonalizeScreen(
    paddingValues: PaddingValues,
    mainNavController: NavController<ScreenDestination>,
    sheetNavController: NavController<SheetDestination>
) {
    AppScaffold(
        topBar = {
            AppCenterTopBar(title = { Text(text = stringResource(id = R.string.personalize)) })
        },
        modifier = Modifier.fillMaxSize()
    ) {
        LazyColumn(modifier = Modifier.fillMaxSize(), contentPadding = paddingValues) {
            item {
                RoundColumn {
                    ItemSwitcher(
                        icon = {
                            Icon(
                                imageVector = Icons.AutoMirrored.Rounded.Label,
                                contentDescription = null
                            )
                        },
                        text = { Text(text = stringResource(id = R.string.sound_quality_label)) },
                        subText = { Text(text = stringResource(id = R.string.sound_quality_label_info)) },
                        checked = SettingRepository.EnableShowSoundQualityLabel,
                        onCheckedChange = {
                            SettingRepository.EnableShowSoundQualityLabel = it
                        }
                    )
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        ItemDivider()
                        ItemSwitcher(
                            icon = {
                                Icon(
                                    imageVector = Icons.Rounded.BorderStyle,
                                    contentDescription = null
                                )
                            },
                            text = { Text(text = stringResource(id = R.string.match_screen_rounded_corners)) },
                            subText = { },
                            checked = SettingRepository.EnableAndroid12ScreenRoundedCorners,
                            onCheckedChange = {
                                SettingRepository.EnableAndroid12ScreenRoundedCorners = it
                            }
                        )
                    }
                }
            }
            item {
                ItemSpacer()
            }
            item {
                RoundColumn {
                    val easterEggSong =
                        AndroidMediaLibRepository.songs.find {
                            it.songName == "着眼" && it.artistName?.contains("双笙") ?: false
                        }
                    SongItem(
                        sheetNavController = easterEggSong?.let { sheetNavController }
                            ?: rememberNavController(initialBackstack = emptyList()),
                        songBean = easterEggSong ?: nothingSong
                    ) {
                        if (easterEggSong != null) {
                            PlayerManager.play(listOf(easterEggSong), 0)
                        }
                    }
                }
            }
            item {
                ItemSpacer()
            }
            item {
                RoundColumn {
                    ItemArrowRight(
                        icon = { Icon(imageVector = Icons.Rounded.Lyrics, null) },
                        text = { Text(text = stringResource(id = R.string.lyric)) },
                        subText = { }) {
                        mainNavController.navigate(ScreenDestination.Lyric)
                    }
                }
            }
            item {
                ItemSpacer()
            }
            item {
                RoundColumn {
                    ItemArrowRight(
                        icon = { Icon(imageVector = Icons.Rounded.Palette, null) },
                        text = { Text(text = stringResource(id = R.string.theme)) },
                        subText = { }) {
                        mainNavController.navigate(ScreenDestination.Theme)
                    }
                }
            }
        }
    }
}