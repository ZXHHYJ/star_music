package com.zxhhyj.music.ui.common

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import com.zxhhyj.ui.theme.LocalDimens
import com.zxhhyj.ui.theme.LocalTextStyles
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import java.util.ArrayDeque

/**
 * 消息提示
 * @author markrenChina
 */
object ComposeToast {

    private val queue = ArrayDeque<String>()
    private val currentMessage = MutableStateFlow<String?>(null)

    fun postToast(message: String) {
        synchronized(queue) {
            queue.add(message)
            if (currentMessage.value == null) {
                currentMessage.value = message
            }
        }
    }

    @Composable
    fun ComposeToast() {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            val stringState = currentMessage.collectAsState()
            AnimatedVisibility(
                visible = stringState.value != null,
                enter = fadeIn(
                    animationSpec = tween(durationMillis = 300)
                ),
                exit = fadeOut(
                    animationSpec = tween(durationMillis = 300)
                )
            )
            {
                val s = remember {
                    stringState.value
                }
                Text(
                    text = s ?: "",
                    color = Color.White,
                    style = LocalTextStyles.current.main,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .background(
                            color = Color.Black.copy(alpha = 0.7f),
                            shape = RoundedCornerShape(LocalDimens.current.round)
                        )
                        .padding(
                            horizontal = LocalDimens.current.horizontal,
                            vertical = LocalDimens.current.vertical
                        )
                )
            }
            LaunchedEffect(stringState.value) {
                delay(1500)
                //移除时去查询队列，取值
                val nextMessage = synchronized(queue) { queue.poll() ?: null }
                if (nextMessage == stringState.value) {
                    currentMessage.value = null
                } else {
                    currentMessage.value = nextMessage
                }
            }
        }
    }
}