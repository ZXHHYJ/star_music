@file:Suppress("UNUSED")

package com.zxhhyj.music.service.playermanager

import android.util.Range
import com.zxhhyj.music.MainApplication
import com.zxhhyj.music.logic.bean.SongBean
import com.zxhhyj.music.service.playermanager.interfaces.Equalizer
import com.zxhhyj.music.ui.common.ComposeToast
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.random.Random

/**
 * 播放管理器
 */
object PlayerManager : Equalizer {

    enum class PlayMode {
        SINGLE_LOOP, LIST_LOOP, RANDOM
    }

    private val androidMusicPlayer = AndroidMusicPlayer(MainApplication.context)

    private val _playMode = MutableStateFlow(PlayMode.LIST_LOOP)
    val playMode: StateFlow<PlayMode> = _playMode

    private val _currentSong = MutableStateFlow<SongBean?>(null)
    val currentSong: StateFlow<SongBean?> = _currentSong

    private val _playList = MutableStateFlow<List<SongBean>?>(null)
    val playlist: StateFlow<List<SongBean>?> = _playList

    private val _indexFlow = MutableStateFlow(-1)
    val index: StateFlow<Int> = _indexFlow

    val duration = androidMusicPlayer.songDurationFlow

    val progress = androidMusicPlayer.currentProgressFlow

    val pause = androidMusicPlayer.pauseFlow

    /**
     * 设置播放器的播放模式
     * @param playMode 播放模式
     */
    fun setPlayMode(playMode: PlayMode) {
        _playMode.value = playMode
    }

    /**
     * 播放歌曲列表中的指定歌曲
     */
    fun play(list: List<SongBean>, index: Int) {
        _playList.value = list
        val song = _playList.value?.getOrNull(index)
        _indexFlow.value = index
        song?.let {
            _currentSong.value = it
            androidMusicPlayer.preparePlay(it.data)
        }
    }

    fun install(list: List<SongBean>, index: Int) {
        _playList.value = list
        list.getOrNull(index)?.let {
            _indexFlow.value = index
            _currentSong.value = it
            androidMusicPlayer.prepare(it.data)
        }
    }

    /**
     * 设置音乐播放进度
     */
    fun seekTo(position: Int) {
        androidMusicPlayer.seekTo(position)
    }

    /**
     * 切换到上一首歌曲
     */
    fun skipToPrevious() {
        val index = _indexFlow.value - 1
        val song = _playList.value?.getOrNull(index)
        song?.let {
            _indexFlow.value = index
            _currentSong.value = it
            androidMusicPlayer.preparePlay(it.data)
        }
    }

    /**
     * 歌曲播放结束时调用
     */
    private fun playbackCompleted() {
        when (_playMode.value) {
            PlayMode.SINGLE_LOOP -> {
                seekTo(0)
                start()
            }

            PlayMode.LIST_LOOP -> {
                val index = _indexFlow.value + 1
                val song = _playList.value?.getOrNull(index)
                song?.let {
                    _indexFlow.value = index
                    _currentSong.value = it
                    androidMusicPlayer.preparePlay(it.data)
                }
            }

            PlayMode.RANDOM -> {
                _playList.value?.let { playlist ->
                    if (playlist.size >= 2) {
                        val randomNumber = if (playlist.size == 2) {
                            if (_indexFlow.value == 0) 1 else 0
                        } else {
                            var newRandomNumber: Int
                            do {
                                newRandomNumber = Random.nextInt(0, playlist.size - 1)
                            } while (newRandomNumber == _indexFlow.value)
                            newRandomNumber
                        }
                        val song = _playList.value?.getOrNull(randomNumber)
                        _indexFlow.value = randomNumber
                        song?.let {
                            _currentSong.value = it
                            androidMusicPlayer.preparePlay(it.data)
                        }
                    }
                }
            }

        }
    }

    /**
     * 切换到下一首歌曲
     */
    fun skipToNext() {
        when (_playMode.value) {
            PlayMode.SINGLE_LOOP, PlayMode.LIST_LOOP -> {
                val index = _indexFlow.value + 1
                val song = _playList.value?.getOrNull(index)
                song?.let {
                    _indexFlow.value = index
                    _currentSong.value = it
                    androidMusicPlayer.preparePlay(it.data)
                }
            }

            PlayMode.RANDOM -> {
                _playList.value?.let { playlist ->
                    if (playlist.size >= 2) {
                        val randomNumber = if (playlist.size == 2) {
                            if (_indexFlow.value == 0) 1 else 0
                        } else {
                            var newRandomNumber: Int
                            do {
                                newRandomNumber = Random.nextInt(0, playlist.size - 1)
                            } while (newRandomNumber == _indexFlow.value)
                            newRandomNumber
                        }
                        val song = _playList.value?.getOrNull(randomNumber)
                        _indexFlow.value = randomNumber
                        song?.let {
                            _currentSong.value = it
                            androidMusicPlayer.preparePlay(it.data)
                        }
                    }
                }
            }

        }
    }

    /**
     * 开始播放音乐
     */
    fun start() {
        androidMusicPlayer.start()
    }

    /**
     * 暂停播放音乐
     */
    fun pause() {
        androidMusicPlayer.pause()
    }

    /**
     * 添加下一首播放的歌曲
     */
    fun addNextPlay(song: SongBean) {
        _playList.value = _playList.value?.toMutableList()?.apply {
            add(_indexFlow.value + 1, song)
        }
    }

    /**
     * 移除指定歌曲
     */
    fun removeSong(song: SongBean) {
        _playList.value?.forEachIndexed { index, songBean ->
            if (songBean == song) {
                _currentSong.value?.takeIf { it == song }?.run {
                    skipToNext()
                }
                _playList.value = _playList.value?.minus(songBean)
                when {
                    index == _indexFlow.value -> {

                    }

                    index > _indexFlow.value -> {

                    }

                    index < _indexFlow.value -> {
                        _indexFlow.value = (_indexFlow.value - 1).coerceAtLeast(0)
                    }
                }
            }
        }
    }

    /**
     * 清空播放列表
     */
    fun clearPlayList() {
        androidMusicPlayer.pause()
        _currentSong.value = null
        _playList.value = null
    }

    override fun setEnableEqualizer(enabled: Boolean) {
        androidMusicPlayer.setEnableEqualizer(enabled)
    }

    override fun setBandLevel(band: Int, level: Int) {
        androidMusicPlayer.setBandLevel(band, level)
    }

    override fun getBandLevel(band: Int): Int {
        return androidMusicPlayer.getBandLevel(band)
    }

    override fun getBandRange(): Range<Int> {
        return androidMusicPlayer.getBandRange()
    }

    override fun getNumberOfBands(): Int {
        return androidMusicPlayer.getNumberOfBands()
    }

    override fun getBandFreqRange(band: Int): IntArray {
        return androidMusicPlayer.getBandFreqRange(band)
    }

    override fun getBand(frequency: Int): Short {
        return androidMusicPlayer.getBand(frequency)
    }

    init {
        androidMusicPlayer.apply {
            completionListener = {
                playbackCompleted()
            }
            errorListener = {
                ComposeToast.postToast(it.errorCodeName)
                pause()
            }
        }
    }

}