package com.zxhhyj.music.service.playermanager.interfaces

import android.util.Range

interface Equalizer {
    /**
     * 设置是否启用均衡器
     * @param enabled 是否启用
     */
    fun setEnableEqualizer(enabled: Boolean)

    /**
     * 设置指定频段的增益水平
     * @param band 频段索引
     * @param level 增益水平
     */
    fun setBandLevel(band: Int, level: Int)

    /**
     * 获取指定频段的增益水平
     * @param band 频段索引
     * @return 增益水平
     */
    fun getBandLevel(band: Int): Int

    /**
     * 获取频段范围
     * @return 频段范围
     */
    fun getBandRange(): Range<Int>

    /**
     * 获取频段数量
     * @return 频段数量
     */
    fun getNumberOfBands(): Int

    /**
     * 获取指定频段的频率范围
     * @param band 频段索引
     * @return 频率范围
     */
    fun getBandFreqRange(band: Int): IntArray

    fun getBand(frequency: Int): Short
}