package com.zxhhyj.music.service.playermanager

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * 播放时长管理器
 */
object PlayerTimerManager {
    sealed class TimerType {
        /**
         * 关闭计时器
         */
        data object Off : TimerType()

        /**
         * 15分钟计时器
         */
        data object Minutes15 : TimerType()

        /**
         * 30分钟计时器
         */
        data object Minutes30 : TimerType()

        /**
         * 45分钟计时器
         */
        data object Minutes45 : TimerType()

        /**
         * 60分钟计时器
         */
        data object Minutes60 : TimerType()

        /**
         * 自定义分钟计时器
         */
        data object CustomMinutes : TimerType()
    }

    var currentTimerType by mutableStateOf<TimerType>(TimerType.Off)
        private set

    var currentRemainingTime by mutableIntStateOf(0)
        private set

    private var timerJob: Job? = null

    /**
     * 启动15分钟计时器
     */
    fun startMinutes15Timer() {
        currentTimerType = TimerType.Minutes15
        customTimer(15 * 60 * 1000)
    }

    /**
     * 启动30分钟计时器
     */
    fun startMinutes30Timer() {
        currentTimerType = TimerType.Minutes30
        customTimer(30 * 60 * 1000)
    }

    /**
     * 启动45分钟计时器
     */
    fun startMinutes45Timer() {
        currentTimerType = TimerType.Minutes45
        customTimer(45 * 60 * 1000)
    }

    /**
     * 启动60分钟计时器
     */
    fun startMinutes60Timer() {
        currentTimerType = TimerType.Minutes60
        customTimer(60 * 60 * 1000)
    }

    /**
     * 启动自定义分钟计时器
     * @param durationInSeconds 计时器时长（秒）
     */
    fun startCustomTimer(durationInSeconds: Int) {
        currentTimerType = TimerType.CustomMinutes
        customTimer(durationInSeconds)
    }

    /**
     * 自定义计时器
     * @param durationInSeconds 计时器时长（秒）
     */
    @OptIn(DelicateCoroutinesApi::class)
    private fun customTimer(durationInSeconds: Int) {
        timerJob = timerJob?.cancel().run {
            GlobalScope.launch(Dispatchers.IO) {
                flow {
                    var remainingTimeInSeconds = durationInSeconds
                    while (true) {
                        emit(remainingTimeInSeconds)
                        delay(1000)
                        remainingTimeInSeconds -= 1000
                    }
                }.collect {
                    currentRemainingTime = it
                    if (it <= 0) {
                        withContext(Dispatchers.Main) {
                            PlayerManager.clearPlayList()
                        }
                        cancelTimer()
                    }
                }
            }
        }
    }

    /**
     * 取消计时器
     */
    fun cancelTimer() {
        currentTimerType = TimerType.Off
        timerJob?.cancel()
    }
}