package com.zxhhyj.music.logic.bean

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SongBean(
    val coverUrl: String?,
    val albumName: String?,
    val artistName: String?,
    val duration: Long?,
    val data: String,
    val dateModified: Long,
    val songName: String,
    val size: Long,
    val id: Long?,
    val bitrate: Int?,
    val samplingRate: Int?,
    val lyric: String?,
    val channels: Int,
    val encoder: String?,
    val genre: String?,
    val bitDepth: Int?,
) : Parcelable