package com.zxhhyj.music.logic.bean

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * @param index 最后播放的歌曲在媒体库中的下标
 * @param songInSongsIndexList 整个歌曲列表在媒体库中的下标
 */
@Parcelize
data class PlayMemoryBean(
    val index: Int? = null,
    val songInSongsIndexList: List<Int>?
) : Parcelable
