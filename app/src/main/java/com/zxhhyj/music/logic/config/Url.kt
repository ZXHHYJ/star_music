package com.zxhhyj.music.logic.config

/**
 * 隐私政策链接
 */
const val PrivacyPolicyURL = "https://docs.qq.com/doc/DU0h6dmRUc0Zqa2x3"

/**
 * 小红书主页
 */
const val RedBookHome = "https://www.xiaohongshu.com/user/profile/65bb64be000000000e0266ff"

/**
 * Gitee项目主页
 */
const val GitProjectHome = "https://gitee.com/ZXHHYJ/star_music"