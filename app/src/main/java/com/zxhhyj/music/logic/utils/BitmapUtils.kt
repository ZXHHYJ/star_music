package com.zxhhyj.music.logic.utils

import android.graphics.Bitmap

object BitmapUtils {
    fun compressBitmap(bitmap: Bitmap): Bitmap {
        val maxPx = 512
        val originalWidth = bitmap.width
        val originalHeight = bitmap.height
        var compressedBitmap = bitmap
        if (originalWidth > maxPx || originalHeight > maxPx) {
            val scaleFactor = (originalWidth / maxPx).coerceAtLeast(originalHeight / maxPx)
            val scaledWidth = originalWidth / scaleFactor
            val scaledHeight = originalHeight / scaleFactor
            compressedBitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth, scaledHeight, true)
        }
        return compressedBitmap.copy(Bitmap.Config.RGB_565, false)
    }
}