package com.zxhhyj.music.logic.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * 将毫秒数转换为分钟:秒钟格式的字符串（Int类型）
 *
 * @return 转换后的分钟:秒钟格式的字符串
 */
fun Int.toTimeString(): String {
    // 将毫秒数转换为秒数
    val s: Int = this / 1000
    // 将秒数转换为分钟:秒钟格式的字符串
    return (s / 60).toString() + ":" + s % 60
}

/**
 * 将毫秒数转换为分钟:秒钟格式的字符串（Long类型）
 *
 * @return 转换后的分钟:秒钟格式的字符串
 */
fun Long.toTimeString(): String {
    // 将毫秒数转换为秒数
    val s = this / 1000
    // 将秒数转换为分钟:秒钟格式的字符串
    return (s / 60).toString() + ":" + s % 60
}

/**
 * 将时间戳转换为字符串格式（yyyy-MM-dd HH:mm:ss）
 *
 * @param timestamp 时间戳（单位：秒）
 * @return 格式化后的时间字符串
 */
fun timestampToString(timestamp: Long): String {
    val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    return format.format(Date(timestamp))
}