package com.zxhhyj.music.logic.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import com.zxhhyj.music.MainApplication

object CopyUtils {
    fun copyText(content: String) {
        val clipboardManager =
            MainApplication.context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
        clipboardManager?.setPrimaryClip(ClipData.newPlainText("Label", content))
    }
}