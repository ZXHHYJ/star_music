package com.zxhhyj.music.logic.config

import com.zxhhyj.music.logic.bean.SongBean

val nothingSong = SongBean(
    "",
    "Nothing",
    "Nothing",
    null,
    "",
    0,
    "Nothing",
    0,
    null,
    128,
    null,
    null,
    0,
    null,
    null,
    null
)