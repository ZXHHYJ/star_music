package com.zxhhyj.music.logic.config

import android.os.Environment
import com.zxhhyj.music.MainApplication

/**
 * 歌曲封面文件夹
 */
val coverFilesDir by lazy { MainApplication.context.getExternalFilesDir("cover") }

/**
 * 内部存储文件夹
 */
val externalStorageDir by lazy { Environment.getExternalStorageDirectory() }